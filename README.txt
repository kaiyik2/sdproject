DeckBuildingGame

Kindly play game using python 2.7

cd into DeckBuildingGame folder and choose from following options.

To run test and play game enter the following command ( use this shell script )
sh BuildAndRun.sh

To run test enter the following command
python -m unittest -v TestDeckBuildingGame


To play game enter the following command
python __init__.py