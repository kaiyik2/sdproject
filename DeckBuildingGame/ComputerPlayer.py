from Player import Player
class ComputerPlayer(Player):
    def __init__(self, PlayerName, PlayerHealth, PlayerHandSize, PlayerStartDeck):
        self.name = PlayerName
        self.health = PlayerHealth
        self.deck = PlayerStartDeck
        self.hand =[]
        self.active = []
        self.handsize= PlayerHandSize
        self.discard = []
        self.transferFromDecktoHand()
        self.money = 0
        self.attack = 0

    def ComputerBuyStuff(self, Central, OpponentOption):
        if OpponentOption == 'A':
            OpponentOption = True
        else:
            OpponentOption = False


        if self.money > 0:
            self.ComputerBuyStuffAI(Central,OpponentOption)
        else:
            print "No money to buy anything"
        self.transferFromHandtoDiscard()
        self.transferFromActivetoDiscard()
        self.transferFromDecktoHand()
        self.money = 0
        self.attack = 0



    def ComputerBuyStuffAI(self,Central,OpponentOption):
        shouldContinue = True
        while shouldContinue == True:
            templist = self.buildingBuyTemplist(Central)
            if len(templist) > 0:
                highestindex = self.identifyStuffToBuy(templist,OpponentOption)
                self.makePurchase(Central,highestindex,templist)
            else:
                shouldContinue = False
            if self.money == 0:
                shouldContinue = False








    def buildingBuyTemplist(self, Central): #building up a temporary list on items that can be afforded
        templist=[]
        if len(Central.supplement)>0:
            if Central.supplement[0].cost <= self.money:
                templist.append(("S", Central.supplement[0]))
        for index in range ( 0, Central.activeSize):
            if Central.active[index].cost <= self.money:
                templist.append((index, Central.active[index]))
        return templist

    def identifyStuffToBuy(self, templist, OpponentOption): #identify cards to  buy

        highestindex = 0
        for index in range (0,len(templist) ):
            if templist[index][1].cost > templist[highestindex][1].cost:
                highestindex = index
            if templist[index][1].cost == templist[highestindex][1].cost:
                if OpponentOption == True : #true is aggresive
                    if templist[index][1].get_attack() > templist[highestindex][1].get_attack():
                        highestindex = index
                else: #Acquisative is chosen
                    if templist[index][1].get_money() > templist[highestindex][1].get_money():
                        highestindex = index
        return highestindex

    def makePurchase(self, Central, highestindex, templist): #Makes actual purchase and call relevant function to make purchase
        source = templist[highestindex][0]
        if source in range(0,5):
            if self.money >= Central.active[int(source)].cost:
                self.deductMoneyActiveDuringPurchase(source,Central)
                self.BuyIndividualCard(source,Central)
        else:
            self.BuySupplementCard(Central)












