import random
from Player import Player
class HumanPlayer(Player):
    def __init__(self, PlayerName, PlayerHealth, PlayerHandSize, PlayerStartDeck): #Initialize Human Setting
        self.name = PlayerName
        self.health = PlayerHealth
        self.deck = PlayerStartDeck
        self.hand =[]
        self.active = []
        self.handsize= PlayerHandSize
        self.discard = []
        self.transferFromDecktoHand()
        self.money = 0
        self.attack = 0


    #def HumanPlayerTurn(self,Central)
    #    self.
    #    self.HumanMainMenuSelection()


    def HumanMainMenuSelection(self): #Ask user about the options that they would like to choose from menu
        inputData = 'D'
        door = False
        while door == False :
            print "\nChoose Action: (P = play all, [0-n] = play that card, B = Buy Card, A = Attack, E = end turn)"

            inputData = raw_input("Enter Action: ")

            if inputData.isdigit():
                if ( int(inputData) < len(self.hand)):
                    door = True
                else:
                    print "Invalid input choose a value between [0-%d]" % (len(self.hand))
            else:
                inputData.upper()
                if inputData == 'P' or inputData == 'B' or inputData == 'A' or inputData == 'E' :
                    door = True

                else:
                    print("Only Input : (P) or [0-n] or (B) or (A) or (E) ")

        self.inputData = inputData
        return inputData

    def Execution(self, options , Central, ComputerPlayer): #Based on chosen options execute functions


        if options.isdigit():
            self.selectionOfCard(options)
            self.printAllCards()
            return False
        elif options == 'P':
            self.selectionOfAllCard()
            print "All Cards Has Been Selected"
            return False
        elif options =='B':
            self.Buy(Central)
            return False
        elif options == 'A':
            print "Human Attacking Computer"
            self.getAttacked(ComputerPlayer)
            return False
        elif options == 'E':
            self.transferFromHandtoDiscard()
            self.transferFromActivetoDiscard()
            self.transferFromDecktoHand()
            self.money = 0
            self.attack = 0
            print ("End of Human Player Turn")
            print ("----------------------------------------------------------------------------------------------------")
            return True


    def Buy(self,Central):    #If buy option is chosen
        door = False
        while door == False:
            options = self.BuyMenu(Central)
            if options.isdigit():
                self.BuyIndividualCard(options,Central)
            elif options == 'S':
                self.BuySupplementCard(Central)
            elif options == 'E':
                door = True





    def BuyMenu(self,Central):   #Prints buy menu and collects user input
        Central.printAllActiveCentral()
        inputData = 'D'
        door = False
        while door == False :
            print "\nChoose a card to buy[0-n], S for supplement, E to end Buying"
            print "Money left : %d" % self.money
            inputData = raw_input("Enter Action: ")
            if inputData.isdigit():
                if ( int(inputData) < len(self.active)):
                    door = True
                else:
                    print "Invalid input choose a value between [0-%d]" % (len(self.active))
            else:
                inputData.upper()
                if inputData == 'S' or inputData == 'E'  :
                    door = True
                    #print("Correct input")
                else:
                    print("Only Input : (S) or [0-n] or (E) ")


        self.inputData = inputData
        return inputData











