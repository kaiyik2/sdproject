from Card import Card
import itertools
class GameProperties(object):
    def __init__(self):
        #Initialize Game settings over here
        self.HumanPlayerName = 'Human Player'
        self.HumanPlayerHealth = 30
        self.HumanPlayerHandSize = 5
        self.ComputerPlayerName = 'Computer Player'
        self.ComputerPlayerHealth = 30
        self.ComputerPlayerHandSize = 5


        self.HumanStartDeck = self.listmaker([8 * [Card('Serf', (0, 1), 0)],2 * [Card('Squire', (1, 0), 0)] ])
        self.ComputerStartDeck = self.listmaker([8 * [Card('Serf', (0, 1), 0)],2 * [Card('Squire', (1, 0), 0)]])
        self.CentralStartCard = self.listmaker(  [4 * [Card('Archer', (3, 0), 2)], 4 * [Card('Baker', (0, 3), 2)], 3 * [Card('Swordsman', (4, 0), 3)], 2 * [Card('Knight', (6, 0), 5)],3 * [Card('Tailor', (0, 4), 3)],3 * [Card('Crossbowman', (4, 0), 3)],3 * [Card('Merchant', (0, 5), 4)],4 * [Card('Thug', (2, 0), 1)],4 * [Card('Thief', (1, 1), 1)],2 * [Card('Catapault', (7, 0), 6)], 2 * [Card('Caravan', (1, 5), 5)],2 * [Card('Assassin', (5, 0), 4)]])
        self.CentralActiveSize = 5
        self.CentralName = 'central'
        self.CentralSupplement  = self.listmaker([10*[Card('Levy', (1,2),2)]])
    def listmaker(self, CardArray):
        return list(itertools.chain.from_iterable(CardArray))
