import random
class Player(object):
    def __init__(self, PlayerName, PlayerHealth, PlayerHandSize, PlayerStartDeck):
        self.name = PlayerName
        self.health = PlayerHealth
        self.deck = PlayerStartDeck
        self.hand =[]
        self.active = []
        self.handsize= PlayerHandSize
        self.discard = []
        self.transferFromDecktoHand()
        self.money = 0
        self.attack = 0


    def shuffle(self, Cards):
        random.shuffle(Cards)

    def transferFromDecktoHand(self): #TransferFromDecktoHand
        for x in range(0, self.handsize):
            #print x
            if(len(self.deck)==0):
                random.shuffle(self.discard)
                self.deck = self.discard
                self.discard = []
            self.hand.append(self.deck.pop())
            #print ("From hand to deck")
            #print self.hand[x]

    def transferFromHandtoDiscard(self):#Transfer from hand to discard
        if (len(self.hand)>0):
            for x in range(0, len(self.hand)):
                self.discard.append(self.hand.pop())


    def transferFromActivetoDiscard(self): #Transfer from active to discard
        if (len(self.active)>0):
            for x in range(0, len(self.active)):

                self.discard.append(self.active.pop())




    def printAllCards(self):  #print all cards that are in hand

        if (len(self.hand)==0):
            print "\nNo Cards Left"
        index = 0
        for card in self.hand:
            print "[%s] %s" % (index, card)
            index = index + 1


    def printMoneyAttack(self): #print money/attack value
        print "\nYour Values"
        print "Money %s, Attack %s" % (self.money,self.attack)

    def selectionOfCard(self, index): #Select card based on index
        if(int(index) < len(self.hand)):
            print "Selected %s" % self.hand[int(index)]
            self.active.append(self.hand.pop(int(index)))
        self.incrementOfMoney(self.active[-1].get_money())

        self.incrementOfAttack(self.active[-1].get_attack())

    def selectionOfAllCard(self): #select all card
        if(len(self.hand)>0):
            for x in range (0, len(self.hand)):
                self.selectionOfCard(0)

    def incrementOfMoney(self, money): #increment money
        self.money = self.money + money
        return self.money

    def incrementOfAttack(self, attack): #increment attack
        self.attack = self.attack + attack
        return self.attack

    def BuyIndividualCard(self,options,Central):  #Buy a card
        if int(options) < len(Central.active):
            if self.money >= Central.active[int(options)].cost:
                self.money = self.deductMoneyActiveDuringPurchase(options, Central)
                print "Bought Card %s" %Central.active[int(options)]
                self.discard.append(Central.active.pop(int(options)))

                if (len(Central.deck)>0):
                    Central.active.append(Central.deck.pop())
                else:
                    Central.activeSize = Central.activeSize - 1
                #print "Card Bought"
            else:
                print "Insufficient money to buy"
        else:
            print "Enter valid index"

    def BuySupplementCard(self, Central): #Buy a supplement card
        if len(Central.supplement) > 0:
            if self.money >= Central.supplement[0].cost:
                self.money = self.deductMoneySupplementDuringPurchase(Central)
                print "Bought Supplement Card "
                self.discard.append(Central.supplement.pop())

            else:
                print "Insufficient money to buy"
        else:
            print "No supplements left"

    def getAttacked(self, ComputerOrHuman):
        print "Attack value is : %s"%self.attack
        ComputerOrHuman.health = ComputerOrHuman.health - self.attack
        self.attack = 0
        return ComputerOrHuman.health






    def deductMoneyActiveDuringPurchase(self, options, Central):
        return self.money - Central.active[int(options)].cost


    def deductMoneySupplementDuringPurchase(self, Central):
        return self.money - Central.supplement[0].cost


