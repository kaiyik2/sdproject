import unittest

from Card import Card
from Game import Game
from GameProperties import GameProperties

class MyTestCase(unittest.TestCase):

    def test_MainMenu(self):
        testGameProperties = GameProperties()
        testGame=Game(testGameProperties)
        self.assertEqual(testGame.HumanPlayer.Execution('E', testGame.Central, testGame.ComputerPlayer),True)

    def test_deductMoneyActiveDuringPurchase(self):
        testGameProperties = GameProperties()
        testGame=Game(testGameProperties)
        testGame.HumanPlayer.money=10
        testGame.Central.active[0].cost=2
        self.assertEqual(testGame.HumanPlayer.deductMoneyActiveDuringPurchase(0,testGame.Central),8)

    def test_deductMoneySupplementDuringPurchase(self):
        testGameProperties = GameProperties()
        testGame=Game(testGameProperties)
        testGame.HumanPlayer.money=10
        self.assertEqual(testGame.HumanPlayer.deductMoneySupplementDuringPurchase(testGame.Central),8)

    def test_getAttacked(self):
        testGameProperties = GameProperties()
        testGame=Game(testGameProperties)
        testGame.HumanPlayer.health=20
        testGame.ComputerPlayer.attack = 10
        self.assertEqual(testGame.ComputerPlayer.getAttacked(testGame.HumanPlayer),10)

    def test_incrementOfAttack(self):
        testGameProperties = GameProperties()
        testGame=Game(testGameProperties)

        testGame.ComputerPlayer.attack=10
        self.assertEqual(testGame.ComputerPlayer.incrementOfAttack(10),20)

    def test_incrementOfAttack(self):
        testGameProperties = GameProperties()
        testGame=Game(testGameProperties)

        testGame.ComputerPlayer.attack=10
        self.assertEqual(testGame.ComputerPlayer.incrementOfAttack(10),20)

    def test_CompareHealth(self):
        testGameProperties = GameProperties()
        testGame=Game(testGameProperties)

        testGame.ComputerPlayer.health=20
        testGame.HumanPlayer.health=10
        self.assertEqual(testGame.CompareHealth(),True)

    def test_DecideWinner(self):
        testGameProperties = GameProperties()
        testGame=Game(testGameProperties)
        testGame.Central.activeSize=5
        testGame.ComputerPlayer.health=10
        testGame.HumanPlayer.health=20
        self.assertEqual(testGame.DecideWinner(),False)




if __name__ == '__main__':
    unittest.main()
