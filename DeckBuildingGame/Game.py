from GameProperties import GameProperties
from Card import Card
from Central import Central
from Player import Player
from HumanPlayer import HumanPlayer
from ComputerPlayer import ComputerPlayer
class Game(object):
    def __init__(self,GameProperties):
        self.GameProperties = GameProperties
        #instantiate Central,HumanPlayer and ComputerPlayer
        self.Central = Central(getattr(self.GameProperties,'CentralName'),getattr(self.GameProperties,'CentralStartCard'),getattr(self.GameProperties,'CentralActiveSize'),getattr(self.GameProperties,'CentralSupplement') )
        self.HumanPlayer = HumanPlayer(getattr(self.GameProperties,'HumanPlayerName'),getattr(self.GameProperties,'HumanPlayerHealth'),getattr(self.GameProperties,'HumanPlayerHandSize'),getattr(self.GameProperties,'HumanStartDeck'))
        self.ComputerPlayer= ComputerPlayer(getattr(self.GameProperties,'ComputerPlayerName'),getattr(self.GameProperties,'ComputerPlayerHealth'),getattr(self.GameProperties,'ComputerPlayerHandSize'),getattr(self.GameProperties,'ComputerStartDeck'))

    def GameDriver(self):
        OpponentOptions=self.selectOpponent()
        gameStillInProgress = True
        while gameStillInProgress == True:

            if self.DecideWinner() == False:  #check if a winner has already been decided
                print "Human Turn"
                self.HumanTurn()

            if self.DecideWinner() == False:  #check if a winner has already been decided
                print "Computer Turn"
                self.ComputerTurn(OpponentOptions)

            if self.DecideWinner() == True:   #check if a winner has already been decided
                if self.AsktoPlayAnotherGame() == True:
                    return True
                else:
                    return False



    def selectOpponent(self):
        inputData = 'D'
        door = False
        while door == False :
            inputData = raw_input('Do you want an aggresive (A) opponent or an Acquisative (Q) opponent. Kindly Enter either (A) or (Q).')
            inputData.upper()
            if inputData == 'A' or inputData == 'Q' :
                door = True

            else:
                print( "Enter only either (A) for aggresive opponent or (Q) for Acquisative opponent")
        if inputData == 'A':
            print("You have selected aggresive opponent")
        elif inputData == 'Q':
            print("You have selected acquisative opponent")
        self.inputData = inputData
        return inputData


    def HumanTurn(self):
        door = False
        while door == False:
            self.PrintHealth()
            self.HumanPlayer.printAllCards()
            self.HumanPlayer.printMoneyAttack()
            options = self.HumanPlayer.HumanMainMenuSelection()   #Print Main Menu and collect input
            door = self.HumanPlayer.Execution(options, self.Central, self.ComputerPlayer)   #Execute options based on inputs

    def PrintHealth(self):
        print "\nPlayer Health %s" % self.HumanPlayer.health
        print "Computer Health %s" % self.ComputerPlayer.health

    def ComputerTurn(self, OpponentOptions):
        self.ComputerPlayer.selectionOfAllCard()
        print("Computer Attacking Human")
        self.ComputerPlayer.getAttacked(self.HumanPlayer)
        self.ComputerPlayer.ComputerBuyStuff(self.Central,OpponentOptions)
        print ("End Of Computer Turn")
        print ("----------------------------------------------------------------------------------------------------")



    def DecideWinner(self):
        if self.HumanPlayer.health <= 0:
            print "Human Player Lose"
            return True

        elif self.ComputerPlayer.health <=0:
            print "Human Player Win"
            return True
        elif self.Central.activeSize ==0:
            self.CompareHealth()
            return True
        else:
            return False





    def CompareHealth(self):
        if self.HumanPlayer.health > self.ComputerPlayer.health:
            print "Human Player Wins on Health"
            return True
        elif self.HumanPlayer.health < self.ComputerPlayer.health:
            print "Computer Player Wins on Health"
            return True
        else:
            print "DRAW"
            return True


    def AsktoPlayAnotherGame(self):
        door = False
        while door == False :
            inputData = raw_input("\nDo You want to play another game ? ( Enter Y for yes and N for no ) :")
            inputData.upper()
            if inputData == 'Y' or inputData == 'N' :
                door = True

            else:
                print( "Enter only either (Y) for Yes or (N) for No")

        if inputData == 'Y':
            return True
        else:
            return False
    def ResetGame(self):
        self.GameProperties.__init__()
        self.Central.__init__(getattr(self.GameProperties,'CentralName'),getattr(self.GameProperties,'CentralStartCard'),getattr(self.GameProperties,'CentralActiveSize'),getattr(self.GameProperties,'CentralSupplement') )
        self.HumanPlayer.__init__(getattr(self.GameProperties,'HumanPlayerName'),getattr(self.GameProperties,'HumanPlayerHealth'),getattr(self.GameProperties,'HumanPlayerHandSize'),getattr(self.GameProperties,'HumanStartDeck'))
        self.ComputerPlayer.__init__(getattr(self.GameProperties,'ComputerPlayerName'),getattr(self.GameProperties,'ComputerPlayerHealth'),getattr(self.GameProperties,'ComputerPlayerHandSize'),getattr(self.GameProperties,'ComputerStartDeck'))
        #self.__init__(self.GameProperties)

