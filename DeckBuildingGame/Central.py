import random
from Card import Card
class Central(object):
    def __init__(self, CentralName, CentralStartCard,CentralActiveSize,CentralSupplement):
        self.name = CentralName
        self.active = []
        self.activeSize = CentralActiveSize
        self.supplement = CentralSupplement
        self.deck = CentralStartCard
        self.shuffleDeck()
        self.transferFromDecktoActive()
    def shuffleDeck(self):
        random.shuffle(self.deck)

    def transferFromDecktoActive(self): #Transfer from central deck to active
        count = 0
        while count < self.activeSize:
            self.active.append(self.deck.pop())
            count = count +1
    def printAllActiveCentral(self):
        index = 0
        for card in self.active:
            print "[%s] %s" % (index, card)
            index = index +1




