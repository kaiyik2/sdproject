from Card import Card
from Game import Game
from GameProperties import GameProperties




if __name__ == '__main__':
    #print Card('test',(1,1),1)
    playagain = True #This is to keep track looping condition
    GameProperties = GameProperties()
    Game=Game(GameProperties) #Each game has a game properties
    while playagain == True:

        playagain = Game.GameDriver() #Start Game Driver

        Game.ResetGame() #Reset Game after a game has ended

    print "Thanks for Playing"




